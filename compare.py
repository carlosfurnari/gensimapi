import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from databaseController import *
from gensim import corpora, models, similarities
from collections import defaultdict
from six import iteritems
from collections import defaultdict
from pprint import pprint  # pretty-printer

def create_dic_corpus():
    conn = create_connection('gensim_sqlite.sqlite')
    cursor = get_all(conn)
    dictionaryLimpio = corpora.Dictionary(row[0].lower().split() for row in cursor)
    dictionaryLimpio.save('dicClean.dict')
    cursor = get_all(conn)
    textsLimpio = [[word for word in ((row[0]).lower().split())]
                   for row in cursor]
    cursor = get_all(conn)
    corpusLimpio = [dictionaryLimpio.doc2bow(text) for text in textsLimpio]
    corpora.MmCorpus.serialize('corpusClean.mm', corpusLimpio)

    stoplist = set('for a of the and to in'.split())
    cursor = get_all(conn)
    textsSwOo1 = [[word for word in row[0].lower().split() if word not in stoplist]
                  for row in cursor]
    frequency = defaultdict(int)
    for text in textsSwOo1:
        for token in text:
            frequency[token] += 1

    textsSwOo1 = [[token for token in text if frequency[token] > 1]
                  for text in textsSwOo1]

    dictionarySwOo1 = corpora.Dictionary(textsSwOo1)
    dictionarySwOo1.save('dicSwOn.dict')
    corpusSwOo1 = [dictionarySwOo1.doc2bow(text) for text in textsSwOo1]
    corpora.MmCorpus.serialize('corpusSwOn.mm', corpusSwOo1)
    return "ok"

def compare_tdidf_clean(text):
    dictionaryLimpio = corpora.Dictionary.load('dicClean.dict')
    corpusLimpio = corpora.MmCorpus('corpusClean.mm')
    tfidfLimpio = models.TfidfModel(corpusLimpio)
    vec_bowLimpio = dictionaryLimpio.doc2bow(text.lower().split())
    vec_tfidfLimpio = tfidfLimpio[vec_bowLimpio]
    indextfidfLimpio = similarities.MatrixSimilarity(
        tfidfLimpio[corpusLimpio])  # transform corpus to LSI space and index it
    simstfidfLimpio = indextfidfLimpio[vec_tfidfLimpio] # perform a similarity query against the corpus
    simstfidfLimpio = sorted(enumerate(simstfidfLimpio), key=lambda item: -item[1])
    return simstfidfLimpio

def compare_tdidf_sw_freq(text):
    dictionarySwOo1 = corpora.Dictionary.load('dicSwOn.dict')
    corpusSwOo1 = corpora.MmCorpus('corpusSwOn.mm')
    tfidfSwOo1 = models.TfidfModel(corpusSwOo1)
    vec_bowSwOo1 = dictionarySwOo1.doc2bow(text.lower().split())
    vec_tfidfSwOo1 = tfidfSwOo1[vec_bowSwOo1]
    indextfidfSwOo1 = similarities.MatrixSimilarity(
        tfidfSwOo1[corpusSwOo1])  # transform corpus to LSI space and index it
    simstfidfSwOo1 = indextfidfSwOo1[vec_tfidfSwOo1] # perform a similarity query against the corpus
    return sorted(enumerate(simstfidfSwOo1), key=lambda item: -item[1])

def compare_tdidf_norm_clean(text):
    dictionaryLimpio = corpora.Dictionary.load('dicClean.dict')
    corpusLimpio = corpora.MmCorpus('corpusClean.mm')
    tfidLimpioNm = models.TfidfModel(corpusLimpio, normalize=True)  # true: normalizacion euclidea del vector
    vec_bowLimpio = dictionaryLimpio.doc2bow(text.lower().split())
    vec_tfidfLimpioNm = tfidLimpioNm[vec_bowLimpio]
    indextfidfLimpioNm = similarities.MatrixSimilarity(
        tfidLimpioNm[corpusLimpio])  # transform corpus to LSI space and index it
    simstfidfLimpioNm = indextfidfLimpioNm[vec_tfidfLimpioNm]  # perform a similarity query against the corpus
    return sorted(enumerate(simstfidfLimpioNm), key=lambda item: -item[1])

def compare_tdidf_norm_sw_freq(text):
    dictionarySwOo1 = corpora.Dictionary.load('dicSwOn.dict')
    corpusSwOo1 = corpora.MmCorpus('corpusSwOn.mm')
    tfidSwOo1Nm = models.TfidfModel(corpusSwOo1, normalize=True)
    vec_bowSwOo1 = dictionarySwOo1.doc2bow(text.lower().split())
    vec_tfidfSwOo1Nm = tfidSwOo1Nm[vec_bowSwOo1]
    indextfidfSwOo1Nm = similarities.MatrixSimilarity(
        tfidSwOo1Nm[corpusSwOo1])  # transform corpus to LSI space and index it
    simstfidfSwOo1Nm = indextfidfSwOo1Nm[vec_tfidfSwOo1Nm]  # perform a similarity query against the corpus
    return sorted(enumerate(simstfidfSwOo1Nm), key=lambda item: -item[1])

def compare_lsi_clean(text):
    dictionaryLimpio = corpora.Dictionary.load('dicClean.dict')
    corpusLimpio = corpora.MmCorpus('corpusClean.mm')
    lsiLimpio = models.LsiModel(corpusLimpio, id2word=dictionaryLimpio,
                                num_topics=300)  # valores recomendables entre 200 y 500
    vec_bowLimpio = dictionaryLimpio.doc2bow(text.lower().split())
    vec_lsiLimpio = lsiLimpio[vec_bowLimpio]
    indexLsiLimpio = similarities.MatrixSimilarity(
        lsiLimpio[corpusLimpio])  # transform corpus to LSI space and index it
    simsLsiLimpio = indexLsiLimpio[vec_lsiLimpio]  # perform a similarity query against the corpus
    return sorted(enumerate(simsLsiLimpio), key=lambda item: -item[1])

def compare_lsi_sw_freq(text):
    dictionarySwOo1 = corpora.Dictionary.load('dicSwOn.dict')
    corpusSwOo1 = corpora.MmCorpus('corpusSwOn.mm')
    lsiSwOo1 = models.LsiModel(corpusSwOo1, id2word=dictionarySwOo1, num_topics=300)
    vec_bowSwOo1 = dictionarySwOo1.doc2bow(text.lower().split())
    vec_lsiSwOo1 = lsiSwOo1[vec_bowSwOo1]  # convert the query to LSI space
    indexLsiSwOo1 = similarities.MatrixSimilarity(
        lsiSwOo1[corpusSwOo1])  # transform corpus to LSI space and index it
    simsLsiSwOo1 = indexLsiSwOo1[vec_lsiSwOo1]  # perform a similarity query against the corpus
    return sorted(enumerate(simsLsiSwOo1), key=lambda item: -item[1])

def compare_lsitfidf_clean(text):
    dictionaryLimpio = corpora.Dictionary.load('dicClean.dict')
    corpusLimpio = corpora.MmCorpus('corpusClean.mm')
    corpusSwOo1 = corpora.MmCorpus('corpusSwOn.mm')
    tfidLimpioNm = models.TfidfModel(corpusLimpio, normalize=True)  # true: normalizacion euclidea del vector
    corpusTfidfLimpio = tfidLimpioNm[corpusLimpio]
    lsiLimpioTfidf = models.LsiModel(corpusTfidfLimpio, id2word=dictionaryLimpio,
                                     num_topics=300)  # valores recomendables entre 200 y 500
    vec_bowLimpio = dictionaryLimpio.doc2bow(text.lower().split())
    vec_lsiTfidfLimpio = lsiLimpioTfidf[vec_bowLimpio]
    indexLsiTtfidfLimpio = similarities.MatrixSimilarity(
        lsiLimpioTfidf[corpusLimpio])  # transform corpus to LSI space and index it
    simsLsiTtfidfLimpio = indexLsiTtfidfLimpio[vec_lsiTfidfLimpio]  # perform a similarity query against the corpus
    return sorted(enumerate(simsLsiTtfidfLimpio), key=lambda item: -item[1])

def compare_lsitfidf_sw_freq(text):
    dictionarySwOo1 = corpora.Dictionary.load('dicSwOn.dict')
    corpusSwOo1 = corpora.MmCorpus('corpusSwOn.mm')
    tfidSwOo1Nm = models.TfidfModel(corpusSwOo1, normalize=True)
    corpusTfidfSwOo1 = tfidSwOo1Nm[corpusSwOo1]
    lsiSwOo1Tfidf = models.LsiModel(corpusTfidfSwOo1, id2word=dictionarySwOo1, num_topics=300)
    vec_bowSwOo1 = dictionarySwOo1.doc2bow(text.lower().split())
    vec_lsiTfidfSwOo1 = lsiSwOo1Tfidf[vec_bowSwOo1]  # convert the query to LSI space
    indexLsiTtfidfSwOo1 = similarities.MatrixSimilarity(
        lsiSwOo1Tfidf[corpusSwOo1])
    simsLsiTtfidfSwOo1 = indexLsiTtfidfSwOo1[vec_lsiTfidfSwOo1]  # perform a similarity query against the corpus
    return sorted(enumerate(simsLsiTtfidfSwOo1), key=lambda item: -item[1])

def compare_rp_clean(text):
    dictionaryLimpio = corpora.Dictionary.load('dicClean.dict')
    corpusLimpio = corpora.MmCorpus('corpusClean.mm')
    corpusSwOo1 = corpora.MmCorpus('corpusSwOn.mm')
    tfidLimpioNm = models.TfidfModel(corpusLimpio, normalize=True)  # true: normalizacion euclidea del vector
    corpusTfidfLimpio = tfidLimpioNm[corpusLimpio]
    rpLimpio = models.RpModel(corpusTfidfLimpio, num_topics=500)
    vec_bowLimpio = dictionaryLimpio.doc2bow(text.lower().split())
    vec_rpLimpio = rpLimpio[vec_bowLimpio]
    indextRpLimpio = similarities.MatrixSimilarity(rpLimpio[corpusLimpio])  # transform corpus to LSI space and index it
    simsRpLimpio = indextRpLimpio[vec_rpLimpio]  # perform a similarity query against the corpus
    return sorted(enumerate(simsRpLimpio), key=lambda item: -item[1])

def compare_rp_sw_freq(text):
    dictionarySwOo1 = corpora.Dictionary.load('dicSwOn.dict')
    corpusSwOo1 = corpora.MmCorpus('corpusSwOn.mm')
    tfidSwOo1Nm = models.TfidfModel(corpusSwOo1, normalize=True)
    corpusTfidfSwOo1 = tfidSwOo1Nm[corpusSwOo1]
    rpSwOo1 = models.RpModel(corpusTfidfSwOo1, num_topics=500)
    vec_bowSwOo1 = dictionarySwOo1.doc2bow(text.lower().split())
    vec_rpSwOo1 = rpSwOo1[vec_bowSwOo1]
    indextRpSwOo1 = similarities.MatrixSimilarity(rpSwOo1[corpusSwOo1])  # transform corpus to LSI space and index it
    simsRpSwOo1 = indextRpSwOo1[vec_rpSwOo1]  # perform a similarity query against the corpus
    return sorted(enumerate(simsRpSwOo1), key=lambda item: -item[1])

def compare_lda_clean(text):
    dictionaryLimpio = corpora.Dictionary.load('dicClean.dict')
    corpusLimpio = corpora.MmCorpus('corpusClean.mm')
    ldaLimpio = models.LdaModel(corpusLimpio, id2word=dictionaryLimpio, num_topics=100)
    vec_bowLimpio = dictionaryLimpio.doc2bow(text.lower().split())
    vec_ldaLimpio = ldaLimpio[vec_bowLimpio]
    indextLdaLimpio = similarities.MatrixSimilarity(ldaLimpio[corpusLimpio])
    simsLdaLimpio = indextLdaLimpio[vec_ldaLimpio]  # perform a similarity query against the corpus
    return sorted(enumerate(simsLdaLimpio), key=lambda item: -item[1])

def compare_lda_sw_freq(text):
    dictionarySwOo1 = corpora.Dictionary.load('dicSwOn.dict')
    corpusSwOo1 = corpora.MmCorpus('corpusSwOn.mm')
    ldaSwOo1 = models.LdaModel(corpusSwOo1, id2word=dictionarySwOo1, num_topics=100)
    vec_bowSwOo1 = dictionarySwOo1.doc2bow(text.lower().split())
    vec_ldaSwOo1 = ldaSwOo1[vec_bowSwOo1]
    indextLdaSwOo1 = similarities.MatrixSimilarity(ldaSwOo1[corpusSwOo1])
    simsLdaSwOo1 = indextLdaSwOo1[vec_ldaSwOo1]  # perform a similarity query against the corpus
    return sorted(enumerate(simsLdaSwOo1), key=lambda item: -item[1])