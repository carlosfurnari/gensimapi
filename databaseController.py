import sqlite3
import os.path
from aifc import Error

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, db_file)
        conn = sqlite3.connect(db_path)
        return conn
    except Error as e:
        print(e)

    return None

def create_req(conn, requisite):
    """
    Create a new project into the projects table
    :param conn:
    :param requisite:
    :return: requisite id
    """
    sql = ''' INSERT INTO requirements(id,req)
              VALUES(?,?) '''
    cur = conn.cursor()
    cur.execute(sql, requisite)
    conn.commit()
    return cur.lastrowid

def update_req(conn, requisite):
    """
    Update project into the projects table
    :param conn:
    :param requisite:
    :return: requisite id
    """
    sql = ''' UPDATE requirements
              SET req = ?
              WHERE id = ? '''
    cur = conn.cursor()
    cur.execute(sql, requisite)
    conn.commit()

def delete_all(conn):
    """
    Create a new project into the projects table
    :param conn:
    :return:
    """
    sql = ''' DELETE FROM requirements '''
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()

def delete_req(conn, requisite):
    """
    Create a new project into the projects table
    :param conn:
    :return:
    """
    sql = ''' DELETE FROM requirements WHERE id = ? '''
    cur = conn.cursor()
    cur.execute(sql, requisite)
    conn.commit()

def req_exists(conn, requisite):
    """
    Create a new project into the projects table
    :param conn:
    :param requisite:
    :return:
    """
    sql = ''' SELECT * FROM requirements WHERE id = ?'''
    cur = conn.cursor()
    cur.execute(sql, requisite)
    return len(cur.fetchall())

def get_all(conn):
    """
    Create a new project into the projects table
    :param conn:
    :return:
    """
    sql = ''' SELECT req FROM requirements '''
    cur = conn.cursor()
    cur.execute(sql)
    return cur