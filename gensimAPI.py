import sys
from flask import Flask
from flask import request
import json
import pandas as pd

from databaseController import *
from compare import *

if getattr(sys, 'frozen', False):
    template_folder = os.path.join(sys.executable, '..', 'templates')
    static_folder = os.path.join(sys.executable, '..', 'static')
    app = Flask(__name__, template_folder=template_folder, \
                static_folder=static_folder)
else:
    app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/gensim/database', methods=['POST'])
def load_db():
    try:
        conn = create_connection('gensim_sqlite.sqlite')
        request_data = request.get_json()
        delete_all(conn)
        for issue in request_data:
            insert = (int(issue["id"]) , issue["name"])
            insert_id = create_req(conn, insert)
        create_dic_corpus()
        conn.close()
        response = {'status':200, 'message':'Database loaded successfully'}
    except:
        response = {'status': 400, 'message': 'Error in loading database'}
    return json.dumps(response)

@app.route('/gensim/database/add', methods=['POST'])
def add_db():
    try:
        conn = create_connection('gensim_sqlite.sqlite')
        request_data = request.get_json()
        select = (int(request_data["id"]),)
        print(select)
        select_exists = req_exists(conn, select)
        print(select_exists)
        if (select_exists == 1):
            update = (request_data["name"], int(request_data["id"]))
            update_ret = update_req(conn, update)
            print(update_ret)
        else:
            insert = (int(request_data["id"]), request_data["name"])
            insert_id = create_req(conn, insert)
            print(insert_id)
        conn.close()
        msg = 'Requirement added correctly to the database: ' +  request_data["name"]
        response = {'status': 200, 'message': msg}
    except:
        msg = 'Error in adding the requirement : ' + request_data["name"]
        response = {'status': 400, 'message': msg}
    return json.dumps(response)

@app.route('/gensim/database/delete/<id>', methods=['POST'])
def delete_db(id):
    try:
        conn = create_connection('gensim_sqlite.sqlite')
        delete_req(conn, id)
        conn.close()
        response = {'status': 200, 'message': 'Requirement deleted  correctly from the database'}
    except:
        response = {'status': 400, 'message': 'Error in loading database'}
    return json.dumps(response)

@app.route('/gensim/test', methods=['GET'])
def test():
    return "OK"


@app.route('/gensim/compareToDb', methods=['POST'])
def compareToDb():
    request_data = request.get_json()
    itext = request_data["requirement"]
    print(itext)
    swoo = request_data["swoo"]
    tdidf = request_data["tfidf"]
    tdidf_norm = request_data["tfidf-norm"]
    lsi = request_data["lsi"]
    lsi_tdidf = request_data["lsi-tfidf"]
    rp = request_data["rp"]
    lda = request_data["lda"]
    objs = []
    if (tdidf == "true"):
        if (swoo == "true"):
            results = compare_tdidf_sw_freq(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "tdidf_sw_freq"}
                    objs.append(d)
        else:
            results = compare_tdidf_clean(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "tdidf_clean"}
                    objs.append(d)
    if (tdidf_norm == "true"):
        if (swoo == "true"):
            results = compare_tdidf_norm_sw_freq(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "tdidf_norm_sw_freq"}
                    objs.append(d)
        else:
            results = compare_tdidf_norm_clean(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "tdidf_norm_clean"}
                    objs.append(d)
    if (lsi == "true"):
        if (swoo == "true"):
            results = compare_lsi_sw_freq(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "lsi_sw_freq"}
                    objs.append(d)
        else:
            results = compare_lsi_clean(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "lsi_clean"}
                    objs.append(d)
    if (lsi_tdidf == "true"):
        if (swoo == "true"):
            results = compare_lsitfidf_sw_freq(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "lsitdidf_sw_freq"}
                    objs.append(d)
        else:
            results = compare_lsitfidf_clean(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim":str(sim), "comparer": "lsitdidf_clean"}
                    objs.append(d)
    if (rp == "true"):
        if (swoo == "true"):
            results = compare_rp_sw_freq(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "rp_sw_freq"}
                    objs.append(d)
        else:
            results = compare_rp_clean(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (a[1] > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "rp_clean"}
                    objs.append(d)
    if (lda == "true"):
        if (swoo == "true"):
            results = compare_lda_sw_freq(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "lda_sw_freq"}
                    objs.append(d)
        else:
            results = compare_lda_clean(itext)
            result2 = pd.Series(results).to_json(orient='values')
            for a in results:
                sim = ((a[1] + 1) / 2)
                if (sim > 0.001):
                    d = {"req": str((a[0]+1)), "sim": str(sim), "comparer": "lda_clean"}
                    objs.append(d)
    '''
    compare_tdidf_sw_freq(text)
    compare_tdidf_norm_clean(text)
    compare_tdidf_norm_sw_freq(text)
    compare_lsi_clean(text)
    compare_lsi_sw_freq(text)
    compare_lsitfidf_clean(text)
    compare_lsitfidf_sw_freq(text)
    compare_rp_clean(text)
    compare_rp_sw_freq(text)
    compare_lda_clean(text)
    compare_lda_sw_freq(text)
    '''
    return json.dumps(objs)

if __name__ == '__main__':
    app.run()